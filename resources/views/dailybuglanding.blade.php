<!DOCTYPE html>
<html>
<head>
	<title>DailyBug</title>
	<!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
	<div class="d-flex vh-100">
		<div class="vh-100 w-50 align-items-center d-flex justify-content-end" style="background-color: #a1795a; color: #08051b">
			<h1>DAILY</h1>
		</div>
		<div class="d-flex vh-100 w-50 align-items-center" style="background-color: #08051b; color: #a1795a">
			<h1>BUG</h1>
		</div>
	</div>
</body>
</html>