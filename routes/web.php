<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dailybuglanding');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// for all
// for all bugs
Route::get('/allbugs', 'BugController@index');

Route::get('/indivbug/{id}', 'BugController@showIndivBug');

// user route
// to show add bug form
Route::get('/addbug', 'BugController@create');

// to save
Route::post('/addbug', 'BugController@store');

// show user bugs
Route::get('/mybugs', 'BugController@indivBugs');

// to delete bugs
Route::delete('/deletebug/{id}', 'BugController@destroy');

// to edit form
Route::get('/editbug/{id}', 'BugController@edit');

// to save edited bug
Route::patch('/editbug/{id}', 'BugController@update');

// to accept solution
Route::patch('/accept/{id}', 'BugController@accept');

// Admin route
// to solve form
Route::get('/solve/{id}', 'BugController@showSolve');

// to save
Route::post('/solve', 'BugController@saveSolution');

// to delete solution
Route::delete('/deletesolution/{id}', 'BugController@delsolution');







