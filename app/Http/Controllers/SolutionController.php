<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SolutionController extends Controller
{
    public function destroy($id){
    	$solutionToDelete = Solution::find($id);
    	$findBug = $solutionToDelete->bug_id;
    	$solutionToDelete->delete();

    	return redirect()->back();
    }
}
