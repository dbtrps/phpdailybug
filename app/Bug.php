<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
use App\Status;
use App\User;

class Bug extends Model
{
    function Category(){
    	return $this->belongsTo('App\Category');
    }

    function Status(){
    	return $this->belongsTo('App\Status');
    }

    function User(){
    	return $this->belongsTo('App\User');
    }
}
